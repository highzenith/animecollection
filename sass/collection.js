function init() {
    $.getJSON("sword-art-online.anime", function(anime) {
        $("#title").append(anime.title);
        $("#demographic a").append(anime.demographic);
        $("#genre a").append(anime.genre);
        $("#creator a").append(anime.creator);
        // $("#initial-medium a").append(getInitialMedium(anime.media));
        var $setting = anime.setting;
        var $bgFileName = "url(\"../images/scifi.png\")";
        $("html, body").css({
            "background": $bgFileName + "fixed"
        });
    });
}

function getInitialMedium(media) {
    var initialMedium;
    _.each(media, function(medium) {
        if(medium.isInitial == true) {
            initialMedium = medium.mediaType;
        }
        return medium.mediaType;
    });
    return initialMedium;
}

//  Calls the initializing function when the page loads.
$(init);